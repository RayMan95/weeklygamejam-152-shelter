﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int health = 100;
    public float speed = 2.0f;

    private Animator animator;
    private bool isRunning = false;

    private Rigidbody2D rb2d;
    // Start is called before the first frame update
    void Start()
    {
        Application.targetFrameRate = 60;
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        float xChange = Input.GetAxis("Horizontal") * speed;
        float yChange = Input.GetAxis("Vertical") * speed;
        
        if(!isRunning && (xChange > 0 || yChange > 0))
        {
            isRunning = true;
        }
        else if(isRunning && (xChange == 0 && yChange == 0))
        {
            isRunning = false;
        }

        animator.SetBool("isRunning", isRunning) ;
        transform.Translate(xChange, yChange, 0f);

    }
}
